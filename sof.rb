require 'nokogiri'
require 'open-uri'

# The gap between columns
COLUMN_GAP = 3

# Class to fetch top questions from StackOverflow
################################################################################

class StackOverflow

  def top_questions()
    page = Nokogiri::HTML(open('https://stackoverflow.com/'))
    page.css('div#question-mini-list').css('div').map { |div|
      {
        :text => div.css('div[class=summary]').css('h3').css('a[class=question-hyperlink]').text,
        :views => div.css('div[class=cp]').css('div[class=views]').css('span').text.to_i
      }
    }.select { |rec| !rec[:text].empty?}
  end

end

# Class reporesenting the grid
################################################################################

class Grid

  class Column

    attr_reader :width
    attr_reader :title

    def with_width(width)
      @width = width
    end

    def with_title(title)
      @title = title
    end

  end

  def initialize
    @columns = []
  end

  def column
    c = Column.new
    @columns << c
    yield c
    self
  end

  def data(data)
    @data = data
    self
  end

  def render
    render_header
    puts
    render_data
  end

  def render_header
    @columns.each { |col|
      print col.title.ljust(col.width, "-")
      print " " * COLUMN_GAP
    }
  end

  def render_data
    @data.each { |row|
      row.keys.each_with_index { |key, index|
        print row[key].to_s[0..@columns[index].width - 1].ljust(@columns[index].width)
        print " " * COLUMN_GAP
      }
      puts
    }
  end

end

# The script
################################################################################

Grid.new
  .column { |c|
    c.with_width(80)
    c.with_title('TOPIC')
  }
  .column { |c|
    c.with_width(20)
    c.with_title('VIEWS')
  }
  .data(StackOverflow.new.top_questions.sort_by { |rec| -rec[:views]} )
  .render()
